# README #

pif - Perl Include Formatter for C and C++

### Short description ###

* The script makes traverses path recursively and sorts include directives in C and C++ source files
* Version 0.4
* Updated 23.12.2015

### New in this version ###

Add config file 

### How to use ###

*Need Perl 5.10 or higer*

pif.pl [key] [path]

Default path is current directory 

Keys list: 

* -h --help	say help
* -b --backup  make file backups (filename.bak)
* -g --group	hash value for custom headers group (groupno=pattern) can appear multiple times 
* -i --ignore	ignore config files

### Description ###

The script traverses path recursively and sorts include directives in C and C++ source files in the following order:

1. own file header (groupno = 100) *to change groupno use -g newgroupno=-own*

1. C headers (groupno = 200) *to change groupno use -g newgroupno=-cstd*

1. C++ headers (groupno = 300) *to change groupno use -g newgroupno=-cppstd*


1. lib headers with prefix (for example wx/ or boost/ ) (groupno = 400) *to change groupno use -g newgroupno=-lib*


1. other system headers (groupno = 500) *to change groupno use -g newgroupno=-system*


1. own written headers (groupno = 600) *to change groupno use -g newgroupno=-local*


Also available custom headers group which defined by key -g groupno=pattern 

*(for examle -g  101=lib  -g 305=math all headers with lib in path/name fall into 101 group, all headers wich math in path/name fall into 305 group. Group 101 will be between own file header (groupno = 100) and C headers (groupno = 200). Group 305 will be between C++ headers (groupno = 300) and different libs headers (groupno = 400)* 

### Config file ###

Script looks for *.pif* in each subdirectory. If the file found and -i or --ignore are not used. Its lines are interpreted as key=value pairs(like in -g parameter) where key represented group number and value is a group name.

For example 

105=math

700=-own

### Developers ###

* SanLe
* xAizek

### Contacts ###

* Developed by Alexandr Lebedinskiy aka SanLe
* [sanle@openmailbox.org](mailto:sanle@openmailbox.org)